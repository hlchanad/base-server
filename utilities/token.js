
var jwt = require('jwt-simple');

module.exports = function(CONFIG){
    return {

        /**
         * generate access token with some data
         *
         * @param data
         * @param duration (unit: hours)
         * @returns {String}
         */
        generateToken: function(data, duration) {

            if (!duration) { duration = 0; }

            var payload = {
                iss: CONFIG.APP_NAME,
                iat: new Date(),
                // exp: new Date((new Date()).setSeconds((new Date()).getSeconds() + duration)), // for testing purpose
                exp: new Date((new Date()).setHours((new Date()).getHours() + duration)),
                typ: 'accessToken',
                data: data
            };

            return jwt.encode(payload, CONFIG.AUTH.ACCESS_TOKEN.JWT_SECRET);
        },

        /**
         * parse token generated above, and validate if it expires
         *
         * @param token
         */
        parseToken: function(token) {

            try{
                var payload = jwt.decode(token, CONFIG.AUTH.ACCESS_TOKEN.JWT_SECRET);

                if (!payload || (new Date(payload.exp)).getTime() < (new Date()).getTime()) {
                    return false;
                }

                return payload;

            } catch (e) {
                return false;
            }
        }
    };
};