
module.exports = {
    'required': 'field_not_found:{field}',
    'allowOnly': 'bad_param:{field}',
    'extraProp': 'extra_prop_not_allowed:{field}'
};
