
var fs = require('fs');
var Config = require('./config.js');

module.exports = function() {
    console.log('storage-local constructor');

    return {
        isObjectExist: function(folder, filename, extension) {
            return new Promise(function(resolve, reject) {
                var path = Config.Local.basePath + folder + filename + '.' + extension;
                resolve(fs.existsSync(path));
            });
        },

        storeBase64Image: function(imageBuffer, folder, filename, extension, isPrivate) {
            return new Promise(function(resolve, reject) {
                var path = Config.Local.basePath + folder + filename + '.' + extension;

                fs.writeFile(path, imageBuffer, 'base64', function(err) {
                    if (err) { reject(err); }
                    else { resolve('fake_link'); } // TODO resolve a real link back to the caller
                });
            });
        }
    }
}