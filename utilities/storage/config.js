
module.exports = {
    General: {
        MethodAllowed: {
            S3: 's3',
            Local: 'local'
        },
        DefaultMethod: 's3',
        imageFolder: 'images/'
    },
    S3: {
        bucketName: 'hlchanad-project-bake',
        baseUrl: 'https://s3-ap-southeast-1.amazonaws.com/'
    },
    Local: {
        basePath: 'uploads/project-bake/' // act as bucket
    }
};