
var HttpStatus = require('http-status-codes');

var Util = require('../util.js');
var localStorage = require('./storage-local.js');
var s3Storage = require('./storage-s3.js');
var Response = require('../response.js');

module.exports = function(CONFIG) {

    var instance;
    switch (CONFIG.GENERAL.METHOD) {
        case CONFIG.GENERAL.METHOD_ALLOWED.S3:    instance = s3Storage();    break;
        case CONFIG.GENERAL.METHOD_ALLOWED.LOCAL: instance = localStorage(); break;
    }

    return {
        isObjectExist: function(folder, filename, extension) {
            return new Promise(function(resolve, reject) {
                instance.isObjectExist(folder, filename, extension)
                    .then(function(isExist) {
                        resolve(isExist);
                    })
                    .catch(function(error) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    });
            });
        },

        /**
         * return `{ link: link, payload: payload }` if payload is present
         * return `link` if payload is absent
         *
         * @param dataString
         * @param folder
         * @param filename
         * @param isPrivate
         * @param payload
         */
        storeBase64Image: function(dataString, folder, filename, isPrivate, payload) {
            var _this = this;

            return new Promise(function(resolve, reject) {
                var imageBuffer = Util.decodeBase64Image(dataString);

                filename = filename ? filename : 'upload_' + (new Date()).getTime();
                folder = folder ? folder : CONFIG.GENERAL.IMAGE_FOLDER;

                _this.isObjectExist(folder, filename, imageBuffer.extension)
                    .then(function(isExist) {
                        filename += isExist ? '-' + Util.randomString(6) : '';

                        instance.storeBase64Image(imageBuffer.data, folder, filename, imageBuffer.extension, isPrivate)
                            .then(function(link) {
                                if (payload !== undefined) {
                                    resolve({ link: link, payload: payload });
                                }
                                else {
                                    resolve(link);
                                }
                            })
                            .catch(function(error) { reject(error); });
                    })
                    .catch(function(error) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    });
            });
        }
    }
}