var HttpStatus = require('http-status-codes');

var Response = require('./response');

module.exports = function (UserConstants, CONFIG) {

    var Token = require('./token')(CONFIG);

    return {
        /**
         * no auth, but if x-access-token is present, still check validity
         *
         * @param req
         * @param res
         * @param next
         */
        noAuth: function (req, res, next) {

            if (req.headers['x-access-token'] === undefined) {
                next();
            }
            else {
                var payload = Token.parseToken(req.headers['x-access-token']);

                if (!payload || !payload.data.user) {
                    res.status(HttpStatus.UNAUTHORIZED)
                        .json(Response.api(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                    return;
                }

                res.locals.tokenPayload = payload;
                res.locals.user = payload.data.user;

                next();
            }
        },

        userLoginRequired: function (req, res, next) {

            if (req.headers['x-access-token'] === undefined) {
                res.status(HttpStatus.BAD_REQUEST)
                    .json(Response.api(HttpStatus.BAD_REQUEST, 'field_not_found:x-access-token'));
                return;
            }

            var payload = Token.parseToken(req.headers['x-access-token']);

            if (!payload || !payload.data.user) {
                res.status(HttpStatus.UNAUTHORIZED)
                    .json(Response.api(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                return;
            }

            res.locals.tokenPayload = payload;
            res.locals.user = payload.data.user;

            next();
        },

        deviceLoginRequired: function (req, res, next) {

            if (req.headers['x-access-token'] === undefined) {
                res.status(HttpStatus.BAD_REQUEST)
                    .json(Response.api(HttpStatus.BAD_REQUEST, 'field_not_found:x-access-token'));
                return;
            }

            var payload = Token.parseToken(req.headers['x-access-token']);

            if (!payload || !payload.data.device) {
                res.status(HttpStatus.UNAUTHORIZED)
                    .json(Response.api(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                return;
            }

            res.locals.tokenPayload = payload;
            res.locals.device = payload.data.device;

            next();
        },

        adminLoginRequired: function (req, res, next) {

            if (req.headers['x-access-token'] === undefined) {
                res.status(HttpStatus.BAD_REQUEST)
                    .json(Response.api(HttpStatus.BAD_REQUEST, 'field_not_found:x-access-token'));
                return;
            }

            var payload = Token.parseToken(req.headers['x-access-token']);

            if (!payload || !payload.data.user) {
                res.status(HttpStatus.UNAUTHORIZED)
                    .json(Response.api(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                return;
            }

            if (payload.data.user.role !== UserConstants.UserRole.Admin) {
                res.status(HttpStatus.FORBIDDEN)
                    .json(Response.api(HttpStatus.FORBIDDEN, 'forbidden'));
                return;
            }

            res.locals.tokenPayload = payload;
            res.locals.user = payload.data.user;

            next();
        }
    };
};