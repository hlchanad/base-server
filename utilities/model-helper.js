
module.exports = {

    isValidObjectId: function(objectId) {
        return objectId.match(/^[a-fA-F0-9]{24}$/);
    },

    isValidBase64Format: function(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        return matches && matches.length === 3;
    },

    isValidUrlFormat: function(url) {
        return url.match(/^https?:\/\/.+$/);
    }
};