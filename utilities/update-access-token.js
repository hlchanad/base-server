
module.exports = function(USER_CONSTANTS, CONFIG) {
    var Token = require('./token')(CONFIG);

    return function (req, res, next) {
        console.log('update-access-token.js CONFIG', CONFIG);

        if (req.headers['x-access-token'] === undefined) {
            next(); // nothing need to do if token is absent
        }
        else {
            // maybe there is another kind of login using x-access-token later

            if (res.locals.user) {
                var duration = res.locals.user.role === USER_CONSTANTS.USER_ROLE.ADMIN ?
                        CONFIG.AUTH.ACCESS_TOKEN.DURATION.ADMIN :
                        CONFIG.AUTH.ACCESS_TOKEN.DURATION.NORMAL,
                    payload = {
                        user: {
                            _id: res.locals.user._id,
                            username: res.locals.user.username,
                            role: res.locals.user.role
                        }
                    },
                    accessToken = Token.generateToken(payload, duration);

                res.locals.responseData.user = {
                    _id: res.locals.user._id,
                    username: res.locals.user.username,
                    role: res.locals.user.role,
                    accessToken: accessToken,
                    expireAt: new Date((new Date()).setHours((new Date()).getHours() + duration))
                };
            }

            next();
        }
    };
};