/**
 * Created by chanhonlun on 5/12/2017.
 */

var HttpStatus = require('http-status-codes');

var FormValidation = require('../../../../utilities/form-validation/form-validation');
var Response = require('../../../../utilities/response.js');
var ModelHelper = require('../../../../utilities/model-helper.js');

module.exports = {
    createNews: function (req, res, next) {
        var formValidation = FormValidation();

        formValidation.allowExtraProp(false);
        formValidation.setRule('title', [{validation: 'required'}]);
        formValidation.setRule('content', [{validation: 'required'}]);
        formValidation.setRule('type', [
            {validation: 'required'},
            {validation: 'allowOnly', allows: ['local', 'international', 'leisure']}]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return;
        }

        next();
    },

    getById: function (req, res, next) {
        if (!ModelHelper.isValidObjectId(req.params.id)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:objectid'));
            return;
        }
        next();
    }
};