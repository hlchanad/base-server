/**
 * Created by chanhonlun on 5/12/2017.
 */

var HttpStatus = require('http-status-codes');

var FormValidation = require('../../../../utilities/form-validation/form-validation');
var Response = require('../../../../utilities/response.js');

module.exports = {

    userCreate: function(req, res, next) {
        var formValidation = FormValidation();

        formValidation.setRule('username', [ { validation: 'required' } ]);
        formValidation.setRule('password', [ { validation: 'required' } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        next();
    },

    userLogin: function(req, res, next) {
        var formValidation = FormValidation();

        formValidation.setRule('username',    [ { validation: 'required' } ]);
        formValidation.setRule('password',    [ { validation: 'required' } ]);
        formValidation.setRule('deviceToken', [ ]);
        formValidation.setRule('deviceType',  [ { validation: 'allowOnly', allows: [ 'iOS', 'Android', 'Web' ] } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        next();
    }
};