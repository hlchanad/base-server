/**
 * Created by chanhonlun on 5/12/2017.
 */

var express = require('express');
var router = express.Router();

var basicValidation = require('./form-validation');
var userValidation = require('./user');
var newsValidation = require('./news');

router.get('/news', basicValidation.noNeed);
router.get('/news/:id', newsValidation.getById);
router.post('/news', newsValidation.createNews);

router.post('/user/create', userValidation.userCreate);
router.post('/user/login', userValidation.userLogin);

module.exports = router;