/**
 * Created by chanhonlun on 5/12/2017.
 */

var express = require('express');
var router = express.Router();

var USER_CONSTANTS = require('../../constants/user'),
    CONFIG = require('../../config/config');
var APIAuth = require('../../../../utilities/api-auth')(USER_CONSTANTS, CONFIG);

router.get('/news', APIAuth.noAuth);
router.post('/news', APIAuth.userLoginRequired);

module.exports = router;