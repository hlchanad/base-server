/**
 * Created by chanhonlun on 5/12/2017.
 */

var USER_CONSTANTS = require('../../constants/user'),
    CONFIG = require('../../config/config');

module.exports = require('../../../../utilities/update-access-token')(USER_CONSTANTS, CONFIG);