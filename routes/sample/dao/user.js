
var Promise = require('promise');
var mongojs = require('mongojs');
var HttpStatus = require('http-status-codes');
var md5 = require('md5');

var Response = require('../../../utilities/response.js');
var USER_CONSTANTS = require('../constants/user.js');

/*
user sample:
user : {
  _id: ObjectId,
  username: string,
  password: string,
  device: {
    token: string,
    type: string   // ios/ android
  }
}
 */

module.exports = function(connection) {

    var COLLECTION = 'user';
    var db = mongojs(connection, [COLLECTION]);

    return {
        getUserByUsernameAndPassword: function(username, password) {
            return new Promise(function(resolve, reject) {
                db[COLLECTION].findOne({ username: username, password: md5(password) }, function(err, user) {
                   if (err) {
                       reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                   }
                   else if (!user) {
                       reject(Response.promiseError(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                   }
                   else {
                       resolve(user);
                   }
                });
            });
        },

        isUsernameUnused: function(username) {
            return new Promise(function(resolve, reject) {
                db[COLLECTION].findOne({ username: username }, function(err, user) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else if (user) {
                        reject(Response.promiseError(HttpStatus.UNAUTHORIZED, 'already_used'));
                    }
                    else {
                        resolve();
                    }
                });
            });
        },

        createUser: function(username, password) {
            return new Promise(function(resolve, reject) {
                var user = {
                    username: username,
                    password: md5(password),
                    role: USER_CONSTANTS.USER_ROLE.NORMAL
                };

                db[COLLECTION].insert(user, function(err) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve();
                    }
                });
            });
        },

        updateUserDeviceTokenAndType: function(user, deviceToken, deviceType) {
            return new Promise(function(resolve, reject) {
                db[COLLECTION].update(
                    { _id: mongojs.ObjectId(user._id) },
                    { $set: { device: { token: deviceToken, type: deviceType} } },
                    function(err) {
                        if (err) {
                            reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                        }
                        else {
                            resolve();
                        }
                    }
                )
            });
        }
    }
};