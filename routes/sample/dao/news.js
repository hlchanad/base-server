/**
 * Created by chanhonlun on 4/12/2017.
 */

var mongojs = require('mongojs');
var Promise = require('promise');
var HttpStatus = require('http-status-codes');

var Response = require('../../../utilities/response.js');

module.exports = function (connection) {

    var COLLECTION = 'news';

    var db = mongojs(connection, [COLLECTION]);

    return {
        getAll: function () {
            return new Promise(function (resolve, reject) {
                db[COLLECTION].find({}, function (err, newses) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(newses || []);
                    }
                });
            });
        },

        getById: function (id) {
            return new Promise(function (resolve, reject) {
                db[COLLECTION].findOne({_id: mongojs.ObjectId(id)}, function (err, news) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else if (!news){
                        reject(Response.promiseError(HttpStatus.NOT_FOUND, 'item_not_found'));
                    }
                    else {
                        resolve(news);
                    }
                });
            });
        },

        create: function (data) {
            return new Promise(function (resolve, reject) {
                db[COLLECTION].insert(data, function (err) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve();
                    }
                });
            });
        }
    };
};