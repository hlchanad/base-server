/**
 * Created by chanhonlun on 4/12/2017.
 */

var CONFIG = require('./config');

var host = CONFIG.DATABASE.HOST,
    port = CONFIG.DATABASE.PORT,
    username = CONFIG.DATABASE.USERNAME,
    password = CONFIG.DATABASE.PASSWORD,
    database = CONFIG.DATABASE.DATABASE,
    connection = 'mongodb://' + username + ':' + password + '@' + host + ':' + port + '/' + database;

if (process.env.IS_HEROKU_APP) {
    connection = process.env.MONGODB_CONNECTION_STRING;
}

console.log('db connection', connection);
module.exports = connection;