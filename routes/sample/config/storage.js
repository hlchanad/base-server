/**
 * Created by chanhonlun on 5/12/2017.
 */

module.exports = {
    GENERAL: {
        METHOD_ALLOWED: {
            S3: 's3',
            LOCAL: 'local'
        },
        METHOD: 's3',
        IMAGE_FOLDER: 'images/'
    },
    S3: {
        BUCKET_NAME: 'hlchanad-project-bake',
        BASE_URL: 'https://s3-ap-southeast-1.amazonaws.com/'
    },
    LOCAL: {
        BASE_PATH: 'uploads/project-bake/' // act as bucket
    }
};