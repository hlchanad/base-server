/**
 * Created by chanhonlun on 4/12/2017.
 */

module.exports = {
    DATABASE: {
        USERNAME: 'sample',
        PASSWORD: 'sample',
        HOST: '127.0.0.1',
        PORT: '27017',
        DATABASE: 'sample'
    },
    APP_NAME: 'Sample Server',
    AUTH: {
        ACCESS_TOKEN: {
            JWT_SECRET: 'this is a secret key!!!!',
            DURATION: {
                ADMIN: 60,
                NORMAL: 60
            }
        }
    }
};