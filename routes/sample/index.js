/**
 * Created by chanhonlun on 4/12/2017.
 */

var connection = require('./config/dbconnect.js');

var express = require('express');
var router = express.Router();

var apiAuthMiddleware = require('./middlewares/api-auth');
var formValidationMiddleware = require('./middlewares/form-validation');
var fileUploadMiddleware = require('./middlewares/file-upload');
var updateAccessTokenMiddleware = require('./middlewares/update-access-token');

var routeHandler = require('./route-handler')(connection);

// init responseData for later use, will be returned at the end
router.use(function(req, res, next) { res.locals.responseData = {}; next(); });

// guard our routes
router.use(apiAuthMiddleware);

// validate req.body in advance
router.use(formValidationMiddleware);

// upload file if needed
router.use(fileUploadMiddleware);

// update token if present
router.use(updateAccessTokenMiddleware);

// --- REAL PATHS ---

router.get('/news', routeHandler.news.getAll);
router.get('/news/:id', routeHandler.news.getById);
router.post('/news', routeHandler.news.create);

router.post('/user/create', routeHandler.user.create);
router.post('/user/login', routeHandler.user.login);

module.exports = router;