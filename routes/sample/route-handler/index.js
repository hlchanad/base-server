/**
 * Created by chanhonlun on 4/12/2017.
 */

module.exports = function (connection) {
    return {
        news: require('./news')(connection),
        user: require('./user')(connection)
    };
};