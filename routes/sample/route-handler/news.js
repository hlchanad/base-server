/**
 * Created by chanhonlun on 4/12/2017.
 */

var HttpStatus = require('http-status-codes')

var Response = require('../../../utilities/response');

module.exports = function (connection) {

    var newsDAO = require('../dao/news')(connection);

    return {
        getAll: function (req, res) {
            newsDAO.getAll()
                .then(function (newses) {
                    res.locals.responseData.newses = newses;
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function (error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        getById: function (req, res) {
            newsDAO.getById(req.params.id)
                .then(function (news) {
                    res.locals.responseData.news = news;
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function (error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                })
        },

        create: function (req, res) {
            newsDAO.create(req.body)
                .then(function () {
                    res.status(HttpStatus.CREATED)
                        .json(Response.api(HttpStatus.CREATED, 'created', res.locals.responseData));
                })
                .catch(function (error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        }
    };
};