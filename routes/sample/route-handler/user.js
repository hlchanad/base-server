
var HttpStatus = require('http-status-codes');

var Response = require('../../../utilities/response.js');
var Token = require('../../../utilities/token.js')(require('../config/config'));
var CONFIG = require('../config/config.js');
var USER_CONSTANTS = require('../constants/user.js');

module.exports = function (connection) {

    var userDAO = require('../dao/user.js')(connection);

    return {

        create: function (req, res) {

            userDAO.isUsernameUnused(req.body.username)
                .then(function() {

                    userDAO.createUser(req.body.username, req.body.password)
                        .then(function() {
                            res.status(HttpStatus.CREATED)
                                .json(Response.api(HttpStatus.CREATED, 'created'));
                        })
                        .catch(function(error) {
                            res.status(error.status)
                                .json(Response.api(error.status, error.message));
                        });
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        login: function (req, res) {

            // Fire a query to your DB and check if the credentials are valid
            userDAO.getUserByUsernameAndPassword(req.body.username, req.body.password)
                .then(function(user) {

                    var duration = user.role === USER_CONSTANTS.USER_ROLE.ADMIN ?
                            CONFIG.AUTH.ACCESS_TOKEN.DURATION.ADMIN :
                            CONFIG.AUTH.ACCESS_TOKEN.DURATION.NORMAL,
                        payload = { user: { _id: user._id, username: user.username, role: user.role } },
                        accessToken = Token.generateToken(payload, duration);

                    var returnUser = {
                        _id: user._id,
                        username: user.username,
                        role: user.role,
                        accessToken: accessToken,
                        expireAt: new Date((new Date()).setHours((new Date()).getHours() + duration))
                    };

                    if (req.body.deviceToken && req.body.deviceType) {
                        userDAO.updateUserDeviceTokenAndType(user, req.body.deviceToken, req.body.deviceType)
                            .then(function() {
                                res.locals.responseData.user = returnUser;
                                res.status(HttpStatus.OK)
                                    .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                            })
                            .catch(function(error) {
                                res.status(error.status)
                                    .json(Response.api(error.status, error.message));
                            });
                    }
                    else {
                        res.locals.responseData.user = returnUser;
                        res.status(HttpStatus.OK)
                            .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                    }
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        }
    };
};