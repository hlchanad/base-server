
var express = require('express');
var router = express.Router();

// separate each request with same newlines
router.use('/', function (req, res, next) {
    console.log("\n");
    next();
});

// router.use('/project-bake', require("./project-bake"));
router.use('/sample', require('./sample'));

module.exports = router;