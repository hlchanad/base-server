# Important Notice
```storage``` module is not tested after restructured

## Development
Select one of the following
- Run `npm start`
- Run `AWS_ACCESS_KEY_ID=xxx AWS_SECRET_ACCESS_KEY=yyy npm start` if you want to use S3 storage instead of local storage

## Deployment
Run `git push heroku master` to deploy on heroku server

## Heroku commands
- Run `heroku logs` to check application logs
- Run `heroku config:set KEY=xxx --remote app_name`
